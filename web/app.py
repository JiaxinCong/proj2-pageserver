from flask import Flask
from flask import render_template

app = Flask(__name__)

@app.route("/<name>")

def hello(name):
    if "~" in name or "//" in name or ".." in name:
        return error_403(403)
    elif name[-4:] == "html":
        try:
            return render_template(name)
        except:
            return error_404(404)
    elif name[-3:] == "css":
        try:
            return render_template(name)
        except:
            return error_404(404)
    else:
        return error_403(403)

@app.errorhandler(404)
def error_404(e):
    return render_template("404.html"),404

@app.errorhandler(403)
def error_403(e):
    return render_template("403.html"),403


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
